import React, { useState, useRef } from 'react';
import { connect, useDispatch } from "react-redux";
import { setWork, setWorkTitle, setWorkCity, setWorkEmoloyer, setWorkStartDate, setWorkStartYr, setWorkEndDate, setWorkEndYr, setWorkDesc }  from './actions'

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const years = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"]

const WorkDetails = (props) => {
    const { work, 
        title,
        city,
        employer,
        startDate,
        startYr,
        endDate,
        endYr,
        desc} = props;
    
    const inputEl = useRef(null);
    const dispatch = useDispatch();

    const handleChange = (e) => {
        const {name, value} = e.target;
        if (name==="title") {
            dispatch(setWorkTitle(value))
        } else if (name==="city") {
            dispatch(setWorkCity(value))
        } else if (name==="employer") {
            dispatch(setWorkEmoloyer(value))
        } else if (name==="startDate") {
            dispatch(setWorkStartDate(value))
        } else if (name==="startYr") {
            dispatch(setWorkStartYr(value))
        } else if (name==="endDate") {
            if (value==="present") {
                console.log(inputEl.current)
                inputEl.current.style.display="none"
            } else {
                inputEl.current.style.display="block"
            }
            dispatch(setWorkEndDate(value))
        } else if (name==="endYr") {
            dispatch(setWorkEndYr(value))
        } else if (name==="desc") {
            dispatch(setWorkDesc(value))
        }
    }

    const handleSave = () => {
        let obj = {
            title,
            city,
            employer,
            startDate,
            startYr,
            endDate,
            endYr,
            desc
        }
        dispatch(setWork(obj))
    }
    return (
        <div className="work-section">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="jobtitle">Job Title</label>
                        <input type="text" name="title" onChange={handleChange} value={title} class="form-control" id="jobtitle"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="workingcity">City/Town</label>
                        <input type="text" name="city" onChange={handleChange} value={city} class="form-control" id="workingcity" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="employer">Employer</label>
                    <input type="text" name="employer" onChange={handleChange} value={employer} class="form-control" id="employer" />
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="startdate">Start Date</label>
                        <div style={{display: "flex"}} id="startdate">
                            <select name="startDate" onChange={handleChange} value={startDate} id="inputState" class="form-control inputState">
                                <option value="" selected disabled>select month</option>
                                {
                                    months.map(month=>(<option>{month}</option>))
                                }
                            </select>
                            <select name="startYr" onChange={handleChange} value={startYr} id="inputState" class="form-control inputState">
                                <option value="" selected disabled>select year</option>
                                {
                                    years.map(yr=>(<option>{yr}</option>))
                                }
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="enddate">End Date</label>
                        <div style={{display: "flex"}} id="enddate">
                            <select name="endDate" onChange={handleChange} value={endDate} id="inputState" class="form-control inputState">
                                <option value="" selected disabled>select month</option>
                                <option >present</option>
                                {
                                    months.map(month=>(<option>{month}</option>))
                                }
                            </select>
                            <select ref={inputEl} name="endYr" onChange={handleChange} value={endYr} id="inputState" class="form-control inputState">
                                <option value="" selected disabled>select year</option>
                                {
                                    years.map(yr=>(<option>{yr}</option>))
                                }
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputCity">Description</label>
                    <textarea name="desc" onChange={handleChange} type="text" value={desc} class="form-control" rows="5" id="inputCity" ></textarea>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = state => ({
    work: state.work,
    title: state.workData.title,
    city: state.workData.workcity,
    employer: state.workData.employer,
    startDate: state.workData.startDate,
    startYr: state.workData.startYr,
    endDate: state.workData.endDate,
    endYr: state.workData.endYr,
    desc: state.workData.desc
});

export default connect(mapStateToProps)(WorkDetails);