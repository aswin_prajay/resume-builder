import React, { useState, useRef } from 'react';
import { MdVisibility } from 'react-icons/md';

import PersonalDetails from './PersonalDetails';
import WorkDetails from './WorkDetails';
import EducationDetails from './EducationDetails';
import Skills from './Skills';
import Resume from './Resume';

const DetailsCardOutLine = (props) => {
    const { label, children } = props;
    return (
        <div className="data-card">
            <header className="card-head">{label}</header>
            <section className="card-body-data">
                {children}
            </section>
        </div>
    )
}


const Design = () => {
    const inputEl = useRef(null);
    const [showPreview, setShowPreview] = useState(false)
    const handlePreview = () => {
        setShowPreview(!showPreview)
        console.log(inputEl.current)
        if (!showPreview) {
            inputEl.current.className = "resume-modal display-block"
        } else {
            inputEl.current.className = "resume-format not-preview"
        }
    }

    return (
        <section className="design-section">
            <div className="input">
                <DetailsCardOutLine label="Personal details">
                    <PersonalDetails />
                </DetailsCardOutLine>
                <DetailsCardOutLine label="Work experience">
                    <WorkDetails /> 
                </DetailsCardOutLine>
                <DetailsCardOutLine label="Education and Qualifications">
                    <EducationDetails /> 
                </DetailsCardOutLine>
                <DetailsCardOutLine label="Skills">
                    <Skills /> 
                </DetailsCardOutLine>
            </div>
            <div className="preview-box">
                <div className="resume-thumbnail">
                    <div ref={inputEl} onClick={handlePreview} className={`resume-format ${"not-preview"}`}>
                        <Resume onClose={handlePreview} preview={showPreview} />
                    </div>
                </div>
                <div className="preview-button">
                    <button onClick={handlePreview}>
                        <MdVisibility className="visibility-icon"/>
                        <span>preview</span>
                    </button>
                </div>
            </div>
        </section>
    )
}

export default Design