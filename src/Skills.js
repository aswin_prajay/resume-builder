import React, { useState } from 'react';

const Skills = (props) => {
    const [state, setState] = useState({
        tags: [
            { id: "HTML", text: "HTML" },
            { id: "CSS", text: "CSS" }
        ],
        suggestions: [
            { id: 'PHP', text: 'PHP' },
            { id: 'JavaScript', text: 'JavaScript' },
            { id: 'React', text: 'React' },
            { id: 'Redux', text: 'Redux' },
            { id: 'Python', text: 'Python' },
            { id: 'Angular', text: 'Angular' }
        ]
    })


    const removeTag = (i) => {
        const newTags = [ ...state.tags ];
        // newTags.splice(i, 1);
        setState({ tags: newTags.filter(elem=>elem.id!==i) });
    }

    const inputKeyDown = (e) => {
        const val = e.target.value;
        if (e.key === 'Enter' && val) {
            if (state.tags.find(tag => tag.text.toLowerCase() === val.toLowerCase())) {
                return;
            }
            setState({ tags: [...state.tags, {id: val, text: val}]});
            // tagInput.value = null;
        } else if (e.key === 'Backspace' && !val) {
          removeTag(state.tags.length - 1);
        }
      }

    return (
        <div className="skill-section">
            <div className="input-tag">
                <ul className="input-tag__tags">
                { state.tags.map((tag, i) => (
                    <li key={tag.id}>
                    {tag.text}
                    <button type="button" onClick={() => { removeTag(tag.id); }}>+</button>
                    </li>
                ))}
                <li className="input-tag__tags__input"><input type="text" onKeyDown={inputKeyDown} ref={c => {  }} /></li>
                </ul>
            </div>
        </div>
    )
}

export default Skills