import React from 'react';
import { connect, useDispatch } from "react-redux";
import { setFirstName, setLastName, setEmail, setPhone, setAddress, setZip, setCity } from './actions'

const PersonalDetails = (props) => {
    const {firstName, lastName, email, phone, address, zip, city} = props
    const dispatch = useDispatch();
    const handleInput = (e) => {
        const {name, value} = e.target;
        // console.log("name ", name, "value", value)
        
        if (name==="firstName") {
            dispatch(setFirstName(value))
        } else if (name==="lastName") {
            dispatch(setLastName(value))
        } else if (name==="email") {
            dispatch(setEmail(value))
        } else if (name==="phone") {
            dispatch(setPhone(value))
        } else if (name==="address") {
            dispatch(setAddress(value))
        } else if (name==="zip") {
            dispatch(setZip(value))
        } else if (name==="city") {
            dispatch(setCity(value))
        }
    }
    console.log("firstName ", firstName)
    return (
        <div className="name-section">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="firstname">First name*</label>
                        <input name="firstName" value={firstName} onChange={handleInput} type="text" class="form-control" id="firstname" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lastname">Last name*</label>
                        <input name="lastName" value={lastName} onChange={handleInput} type="text" class="form-control" id="lastname" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email address</label>
                        <input name="email" value={email} onChange={handleInput} type="text" class="form-control" id="inputEmail4" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phonenumber">Phone number</label>
                        <input name="phone" value={phone} onChange={handleInput} type="text" class="form-control" id="phonenumber" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input name="address" value={address} onChange={handleInput} type="text" class="form-control" id="inputAddress" />
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="zipCode">Zip code</label>
                        <input name="zip" value={zip} onChange={handleInput} type="text" class="form-control" id="zipCode" />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="city">City/Town</label>
                        <input name="city" value={city} onChange={handleInput} type="text" class="form-control" id="city" />
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = state => ({
    firstName: state.personal.firstName,
    lastName: state.personal.lastName,
    email: state.personal.email,
    phone: state.personal.phone,
    address: state.personal.address,
    zip: state.personal.zip,
    city: state.personal.city
});
  
export default connect(mapStateToProps)(PersonalDetails);