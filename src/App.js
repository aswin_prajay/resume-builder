import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from "react-redux";
import thunk from "redux-thunk";


import Home from "./HomeScreen";
import Design from "./Design"
import './App.css';
import reducer from "./reducers";


let store = createStore(reducer, applyMiddleware(thunk))

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <header className="header">
            <nav id="header" className="navbar navbar-expand-lg navbar-light header">
              <a className="navbar-brand" href="/">CV maker</a>
            </nav>
          </header>
            <Route exact path="/" component={Home} />
            <Route exact path="/designs" component={Design} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
