import React, { useRef } from 'react';
import { connect, useDispatch } from "react-redux";
import { setEducation, setEduTitle, setEduCity, setEduSchool, setEduStartDate, setEduStartYr, setEduEndDate, setEduEndYr, setEduDesc }  from './actions'

const months = ["present", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const years = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"]

const EducationDetails = (props) => {
    const { work, 
        title,
        city,
        school,
        startDate,
        startYr,
        endDate,
        endYr,
        desc } = props;
    const dispatch = useDispatch();
    const inputEl = useRef(null);

    const handleChange = (e) => {
        const {name, value} = e.target;
        if (name==="title") {
            dispatch(setEduTitle(value))
        } else if (name==="city") {
            dispatch(setEduCity(value))
        } else if (name==="school") {
            dispatch(setEduSchool(value))
        } else if (name==="startDate") {
            dispatch(setEduStartDate(value))
        } else if (name==="startYr") {
            dispatch(setEduStartYr(value))
        } else if (name==="endDate") {
            if (value==="present") {
                console.log(inputEl.current)
                inputEl.current.style.display="none"
            } else {
                inputEl.current.style.display="block"
            }
            dispatch(setEduEndDate(value))
        } else if (name==="endYr") {
            dispatch(setEduEndYr(value))
        } else if (name==="desc") {
            dispatch(setEduDesc(value))
        }
    }

    return (
        <div className="education-section">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="jobtitle">Degree</label>
                        <input type="text" name="title" value={title} onChange={handleChange} class="form-control" id="jobtitle"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="workingcity">City/Town</label>
                        <input type="text" value={city} name="city" onChange={handleChange} class="form-control" id="workingcity" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="employer">School</label>
                    <input type="text" value={school} name="school" onChange={handleChange} class="form-control" id="employer" placeholder="1234 Main St" />
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="startdate">Start Date</label>
                        <div style={{display: "flex"}} id="startdate">
                            <select id="inputState" value={startDate} name="startDate" onChange={handleChange} class="form-control inputState">
                                <option value="" selected disabled>select month</option>
                                {
                                    months.map(month=>(<option>{month}</option>))
                                }
                            </select>
                            <select id="inputState" value={startYr} name="startYr" onChange={handleChange} class="form-control inputState">
                                <option value="" selected disabled>select year</option>
                                {
                                    years.map(yr=>(<option>{yr}</option>))
                                }
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="enddate">End Date</label>
                        <div style={{display: "flex"}} id="enddate">
                            <select id="inputState" value={endDate} name="endDate" onChange={handleChange} class="form-control inputState">
                                <option value="" selected disabled>select month</option>
                                {
                                    months.map(month=>(<option>{month}</option>))
                                }
                            </select>
                            <select ref={inputEl} id="inputState" value={endYr} name="endYr" onChange={handleChange} class="form-control inputState">
                                <option value="" selected disabled>select year</option>
                                {
                                    years.map(yr=>(<option>{yr}</option>))
                                }
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputCity">Description</label>
                    <textarea type="text" value={desc} name="desc" onChange={handleChange} class="form-control" rows="5" id="inputCity" ></textarea>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = state => ({
    title: state.educationData.edutitle,
    city: state.educationData.educity,
    school: state.educationData.school,
    startDate: state.educationData.edustartDate,
    startYr: state.educationData.edustartYr,
    endDate: state.educationData.eduendDate,
    endYr: state.educationData.eduendYr,
    desc: state.educationData.edudesc
});

export default connect(mapStateToProps)(EducationDetails);