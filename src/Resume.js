import React from 'react';
import { connect, useDispatch } from "react-redux";

function Resume(props) {
    const { 
        onClose, 
        preview,
        firstName,
        lastName,
        email,
        phone,
        address,
        zip,
        city,
        title,
        workcity,
        employer,
        startDate,
        startYr,
        endDate,
        endYr,
        desc,
        edutitle,
        educity,
        school,
        edustartDate,
        edustartYr,
        eduendDate,
        eduendYr,
        edudesc, d
    } = props
    console.log("work ", d)
    console.log(preview)
  return (
        <div onClick={onClose} className={preview?"preview":"mod"} >
            <section className="personal-section">
                <header className={preview?"section-sub1-header":"section-sub-header"}>Personal</header>
                <div className={preview?"section-sub1-body":"section-sub-body"}>
                    <div className="field">Name</div>
                    <div className="field-value">{firstName?firstName+" "+lastName:"Your Name"}</div>
                </div>
                <div className={preview?"section-sub1-body":"section-sub-body"}>
                    <div className="field">Address</div>
                    <div className="field-value">{address? address+" "+zip: "Your Address"}</div>
                </div>
                <div className={preview?"section-sub1-body":"section-sub-body"}>
                    <div className="field">Phone number</div>
                    <div className="field-value">{phone?phone:"98XXXXXX21"}</div>
                </div>
                <div className={preview?"section-sub1-body":"section-sub-body"}>
                    <div className="field">Email</div>
                    <div className="field-value">{email?email:"name@example.com"}</div>
                </div>
            </section>
            <section className="main-section">
                <header className={preview?"main-sub1-header":"main-sub-header"}>{firstName?firstName+" "+lastName:"Your Name"}</header>
                <div className={preview?"main-sub1-body":"main-sub-body"}>
                    <div className="field">Work Experience</div>
                    <div className="field-row">
                        <div className="field-label">
                            <div className="field-mainlabel">{title?title:"Job Title"}</div>
                            <div className="field-date">{`${startDate} ${startYr} - ${endDate==="present"?endDate:endDate+" "+endYr}`}</div>
                        </div>
                        <div className="field-topic">{employer?employer:"Employer Name"}</div>
                        <div className="field-desc">{desc?desc:"Description about employer"}</div>
                    </div>
                </div>
                <div className={preview?"main-sub1-body":"main-sub-body"}>
                    <div className="field">Education and Qualifications</div>
                    <div className="field-row">
                        <div className="field-label">
                            <div className="field-mainlabel">{edutitle?edutitle:"Qualification"}</div>
                            <div className="field-date">{`${edustartDate} ${edustartYr} - ${eduendDate==="present"?eduendDate:eduendDate+" "+eduendYr}`}</div>
                        </div>
                        <div className="field-topic">{school?school:"Institute Name"}</div>
                        <div className="field-desc">{edudesc?edudesc:"Academic points"}</div>
                    </div>
                </div>
            </section>
        </div>
  );
}

const mapStateToProps = state => ({
    d: state,
    firstName: state.personal.firstName,
    lastName: state.personal.lastName,
    email: state.personal.email,
    phone: state.personal.phone,
    address: state.personal.address,
    zip: state.personal.zip,
    city: state.personal.city,
    title: state.workData.title,
    workcity: state.workData.workcity,
    employer: state.workData.employer,
    startDate: state.workData.startDate,
    startYr: state.workData.startYr,
    endDate: state.workData.endDate,
    endYr: state.workData.endYr,
    desc: state.workData.desc,
    edutitle: state.educationData.edutitle,
    educity: state.educationData.educity,
    school: state.educationData.school,
    edustartDate: state.educationData.edustartDate,
    edustartYr: state.educationData.edustartYr,
    eduendDate: state.educationData.eduendDate,
    eduendYr: state.educationData.eduendYr,
    edudesc: state.educationData.edudesc
});
  
export default connect(mapStateToProps)(Resume);
