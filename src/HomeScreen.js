import React from 'react';
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <section className="section">
            <div  className="main">
                <Link to="/designs">
                    <button className="design-button">make your cv now</button>
                </Link>
            </div>
            <div  className="main design">
                
            </div>
        </section>
    )
}

export default Home