import { combineReducers } from "redux";

const initialPersonalState = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    address: "",
    zip: "",
    city: ""
};

const initialWorkData = {
    title: "",
    workcity: "",
    employer: "",
    startDate: "",
    startYr: "",
    endDate: "",
    endYr: "",
    desc: ""
}

const initialWorkState = [];

const initialEducationData = {
    edutitle: "",
    educity: "",
    school: "",
    edustartDate: "",
    edustartYr: "",
    eduendDate: "",
    eduendYr: "",
    edudesc: ""
}

const initialEduState = [{}];

const personal = (state = initialPersonalState, action) => {
    switch(action.type) {
        case "firstName": {
            return {
                ...state,
                firstName: action.payload
            };
        }
        case "lastName": {
            return {
                ...state,
                lastName: action.payload
            };
        }
        case "email": {
            return {
                ...state,
                email: action.payload
            };
        }
        case "phone": {
            return {
                ...state,
                phone: action.payload
            };
        }
        case "address": {
            return {
                ...state,
                address: action.payload
            };
        }
        case "zip": {
            return {
                ...state,
                zip: action.payload
            };
        }
        case "city": {
            return {
                ...state,
                city: action.payload
            };
        }
        default:
            return state;
    }
}

const workData = (state = initialWorkData, action) => {
    switch(action.type) {
        case "title": {
            return {
                ...state,
                title: action.payload
            };
        }
        case "workcity": {
            return {
                ...state,
                workcity: action.payload
            };
        }
        case "employer": {
            return {
                ...state,
                employer: action.payload
            };
        }
        case "startDate": {
            return {
                ...state,
                startDate: action.payload
            };
        }
        case "startYr": {
            return {
                ...state,
                startYr: action.payload
            };
        }
        case "endDate": {
            return {
                ...state,
                endDate: action.payload
            };
        }
        case "endYr": {
            return {
                ...state,
                endYr: action.payload
            };
        }
        case "desc": {
            return {
                ...state,
                desc: action.payload
            };
        }
        default:
            return state;
    }
}

const work = (state = initialWorkState, action) => {
    switch(action.type) {
        case "work": {
            return [...state, action.payload];
        }
        default:
            return state;
    }
}

const educationData = (state = initialEducationData, action) => {
    switch(action.type) {
        case "edutitle": {
            return {
                ...state,
                edutitle: action.payload
            };
        }
        case "educity": {
            return {
                ...state,
                educity: action.payload
            };
        }
        case "school": {
            return {
                ...state,
                school: action.payload
            };
        }
        case "edustartDate": {
            return {
                ...state,
                edustartDate: action.payload
            };
        }
        case "edustartYr": {
            return {
                ...state,
                edustartYr: action.payload
            };
        }
        case "eduendDate": {
            return {
                ...state,
                eduendDate: action.payload
            };
        }
        case "eduendYr": {
            return {
                ...state,
                eduendYr: action.payload
            };
        }
        case "edudesc": {
            return {
                ...state,
                edudesc: action.payload
            };
        }
        default:
            return state;
    }
}

const education = (state = initialEduState, action) => {
    switch(action.type) {
        case "education": {
            return [...state, action.payload];
        }
        default:
            return state;
    }
}

const skills = (state = initialEduState, action) => {
    switch(action.type) {
        case "addSkill": {
            return [...state, action.payload];
        }
        case "deleteSkill": {
            return [...state.filter(elem=>elem.text!==action.payload)];
        }
        default:
            return state;
    }
}
  

export default combineReducers({
    personal,
    work,
    workData,
    education,
    educationData,
    skills
});
  