export const setFirstName = data => dispatch => {
    dispatch({
        type: "firstName",
        payload: data
    });
};

export const setLastName = data => dispatch => {
    dispatch({
        type: "lastName",
        payload: data
    });
};

export const setEmail = data => dispatch => {
    dispatch({
        type: "email",
        payload: data
    });
};

export const setPhone = data => dispatch => {
    dispatch({
        type: "phone",
        payload: data
    });
};

export const setAddress = data => dispatch => {
    dispatch({
        type: "address",
        payload: data
    });
};

export const setZip = data => dispatch => {
    dispatch({
        type: "zip",
        payload: data
    });
};

export const setCity = data => dispatch => {
    dispatch({
        type: "city",
        payload: data
    });
};

export const setWorkTitle = data => dispatch => {
    dispatch({
        type: "title",
        payload: data
    });
};

export const setWorkCity = data => dispatch => {
    dispatch({
        type: "workcity",
        payload: data
    });
};

export const setWorkEmoloyer = data => dispatch => {
    dispatch({
        type: "employer",
        payload: data
    });
};

export const setWorkStartDate = data => dispatch => {
    dispatch({
        type: "startDate",
        payload: data
    });
};

export const setWorkStartYr = data => dispatch => {
    dispatch({
        type: "startYr",
        payload: data
    });
};

export const setWorkEndDate = data => dispatch => {
    dispatch({
        type: "endDate",
        payload: data
    });
};

export const setWorkEndYr = data => dispatch => {
    dispatch({
        type: "endYr",
        payload: data
    });
};

export const setWorkDesc = data => dispatch => {
    dispatch({
        type: "desc",
        payload: data
    });
};

export const setWork = data => dispatch => {
    dispatch({
        type: "work",
        payload: data
    });
};

export const setEduTitle = data => dispatch => {
    dispatch({
        type: "edutitle",
        payload: data
    });
};

export const setEduCity = data => dispatch => {
    dispatch({
        type: "educity",
        payload: data
    });
};

export const setEduSchool = data => dispatch => {
    dispatch({
        type: "school",
        payload: data
    });
};

export const setEduStartDate = data => dispatch => {
    dispatch({
        type: "edustartDate",
        payload: data
    });
};

export const setEduStartYr = data => dispatch => {
    dispatch({
        type: "edustartYr",
        payload: data
    });
};

export const setEduEndDate = data => dispatch => {
    dispatch({
        type: "eduendDate",
        payload: data
    });
};

export const setEduEndYr = data => dispatch => {
    dispatch({
        type: "eduendYr",
        payload: data
    });
};

export const setEduDesc = data => dispatch => {
    dispatch({
        type: "edudesc",
        payload: data
    });
};


export const setEducation = data => dispatch => {
    dispatch({
        type: "education",
        payload: data
    });
};

export const setAddSkill = data => dispatch => {
    dispatch({
        type: "addSkill",
        payload: data
    });
};

export const setDeleteSkill = data => dispatch => {
    dispatch({
        type: "deleteSkill",
        payload: data
    });
};

